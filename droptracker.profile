<?php
/**
 * @file
 * Enables modules and site configuration for a Droptracker installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 */
function droptracker_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = t('Droptracker');
}
